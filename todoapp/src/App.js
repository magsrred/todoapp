import React from 'react';
//import logo from './logo.svg';
import './App.css';
//import { render } from '@testing-library/react';

class App extends React.Component {
//<Item value={"Go Shopping"}/>, <Item value={"Mow Lawn"}/>, <Item value={"Walk Dog"}/>
  constructor(props){
    super(props);
    this.state = {
      list: [],
      listLength: 0,
      value: '',
    }
  }

  handleChange(event){
    this.setState(
      {value: event.target.value}
    );
  }

  handleSubmit(event){
    const textList = this.state.list.slice(0, this.state.listLength + 1);
    //alert("You wrote: " + this.state.value);
    this.setState({
      list: textList.concat(this.state.value),
      listLength: this.state.listLength + 1,
      value: '',
    });
    event.preventDefault();
  }

  renderList(){
    return(
      <ToDoList list={this.state.list} key={this.state.listLength}/>
    );
  }

  render(){
    return(
      <div>
        <Header handleSubmit={(event) => this.handleSubmit(event)} value={this.state.value} handleChange={(event) => this.handleChange(event)}/>
        {this.renderList()}
      </div>
    );
  }
}

class Header extends React.Component {
  render(){
    return(
      <div className="App">
        <header className="App-header">
          <h1>To-Do List</h1>
          <h3>Enter what you're going to do next!</h3>
          <InputForm handleSubmit={(event) => this.props.handleSubmit(event)} text={this.props.value} handleChange={(event) => this.props.handleChange(event)}/>
        </header>
      </div>
    );
  }
}

function InputForm(props){

    return(
      <form onSubmit={props.handleSubmit}>
        <input type="text" value={props.text} onChange={props.handleChange}/>
        <input type="submit" value="Submit" />
      </form>
    );
}

class ToDoList extends React.Component {
  createItemList(textList){
    const itemList = Array(textList.length);
    for(var i=0; i<textList.length; i++){
      itemList[i] = <Item value={textList[i]} done={false}/>;
    }
    return(itemList);
  }
  render(){
    const items = this.createItemList(this.props.list);
    const step = this.props.key;
    const toDoList = items.map((item, step) =>
      <li key={step}>
        {item}
      </li>
    );
    return(
      <div className="App-body">
        <ol>
          {toDoList}
        </ol>
      </div>
    );
  }
}

class Item extends React.Component {
 
  constructor(props){
    super(props);
    this.state = {
      onOff: props.done,
    }
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(value){
    this.setState({
      onOff: !value,
    });
  }
  render(){
    return(
      <div>
        <h4>{this.props.value + "          "}<button className="App-button" onClick={() => this.handleClick(this.state.onOff)}>{this.state.onOff ? 'X' : ' '}</button></h4>
      </div>
    );
  }
}


export default App;